<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class api extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('dbapi');
    }

    public function index()
    {
        echo 'beasiswa api';
    }

    public function LoginApi()
    {
        $code = $this->input->post('code');
        $result = $this->dbapi->LoginApi($code);
        echo json_encode($result);
    }
}