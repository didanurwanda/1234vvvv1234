<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Stores extends MY_Controller
{

    function __construct() {
        parent::__construct();

        if (! $this->loggedIn) {
            redirect('login');
        }

        if (!$this->ion_auth->in_group(array('admin', 'owner'))) {
            $this->session->set_flashdata('error', lang("access_denied"));
            redirect('welcome');
        }
    }

    function index() {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['page_title'] = lang('stores');
        $bc = array(array('link' => '#', 'page' => lang('stores')));
        $meta = array('page_title' => lang('stores'), 'bc' => $bc);
        $this->page_construct('stores', $this->data, $meta);
    }

    function get_stores() {
        $this->load->library('datatables');
        $this->datatables
        ->select("id, name, code, phone, email, address1, city")
        ->from("stores")
        ->add_column("Actions", "<div class='text-center'><a href='" . site_url('stores/select_store/$1') . "' class='tip btn btn-primary btn-xs' title='".$this->lang->line("select_store")."'><i class='fa fa-check-square-o'></i> ".$this->lang->line("select_store")."</a></div>", "id")
        ->unset_column('id');
        echo $this->datatables->generate();
    }

    function select_store($store_id) {
        $this->session->set_flashdata('message', lang("store_selected"));
        $this->session->set_userdata('store_id', $store_id);
        redirect('welcome');
    }

    function deselect_store($store_id = null) {
        $this->session->set_flashdata('message', lang("store_deselected"));
        $this->session->set_userdata('store_id', NULL);
        redirect('welcome');
    }

}
