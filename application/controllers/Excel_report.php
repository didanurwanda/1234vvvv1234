<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Excel_report extends MY_Controller
{

    function __construct() {
        parent::__construct();


        if ( ! $this->loggedIn) {
            redirect('login');
        }

        if ( ! $this->Admin) {
            $this->session->set_flashdata('error', lang('access_denied'));
            redirect('pos');
        }

        $this->load->model('excel_model');
        $this->load->helper('tanggalindonesia');
    }

    function excel(){
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        $this->data['tampil'] = $this->excel_model->excel_report();
        //$this->data['sale'] = $this->reports_model->getSalesDetail($id);
        //print_r($tampil);die();
        $this->page_construct('reports/excel', $this->data);
    }

}
