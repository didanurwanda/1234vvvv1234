<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Carbon\Carbon;

class Reports extends MY_Controller
{

    function __construct() {
        parent::__construct();


        if ( ! $this->loggedIn) {
            redirect('login');
        }

        // for: admin, owner
        if (!$this->ion_auth->in_group(array('admin', 'owner'))) {
            $this->session->set_flashdata('error', $this->lang->line('access_denied'));
            redirect('welcome');
        }

        $this->load->model('reports_model');
    }

    function daily_sales($store_id = 'all', $year = NULL, $month = NULL, $report = 'no')
    {
        if (!$year) { $year = date('Y'); }
        if (!$month) { $month = date('m'); }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->lang->load('calendar');
        $config = array(
            'show_next_prev' => TRUE,
            'next_prev_url' => site_url('reports/daily_sales/'. $store_id),
            'month_type' => 'long',
            'day_type' => 'long'
            );
        $config['template'] = '

        {table_open}<table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" style="min-width:522px;">{/table_open}

        {heading_row_start}<tr class="active">{/heading_row_start}

        {heading_previous_cell}<th><div class="text-center"><a href="{previous_url}">&lt;&lt;</div></a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}"><div class="text-center">{heading}</div></th>{/heading_title_cell}
        {heading_next_cell}<th><div class="text-center"><a href="{next_url}">&gt;&gt;</a></div></th>{/heading_next_cell}

        {heading_row_end}</tr>{/heading_row_end}

        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td class="cl_equal"><div class="cl_wday">{week_day}</div></td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}

        {cal_row_start}<tr>{/cal_row_start}
        {cal_cell_start}<td>{/cal_cell_start}

        {cal_cell_content}<div class="cl_left">{day}</div><div class="cl_right">{content}</div>{/cal_cell_content}
        {cal_cell_content_today}<div class="cl_left highlight">{day}</div><div class="cl_right">{content}</div>{/cal_cell_content_today}

        {cal_cell_no_content}{day}{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="highlight">{day}</div>{/cal_cell_no_content_today}

        {cal_cell_blank}&nbsp;{/cal_cell_blank}

        {cal_cell_end}</td>{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}

        {table_close}</table>{/table_close}
        ';

        $this->load->library('calendar', $config);

        $sales = $this->reports_model->getDailySales($store_id, $year, $month);

        $carbonDate = Carbon::createFromFormat('Y-m-d', $year .'-'. $month .'-01');
        $monthName = lang('cal_'. strtolower($carbonDate->format('F')));

        for($i = 1; $i <= ((int) $carbonDate->format('t')); $i++) {
            $day = $i;
            if ($i < 10) {
                $day = '0'. $i;
            }
            $carbonDate = Carbon::createFromFormat('Y-m-d', $year .'-'. $month .'-'. $day);

            $daily_sale2[$i] = array(
                'date' => $i,
                'day' => lang('cal_'. strtolower($carbonDate->format('l'))),
                'sales' => 0 .' ',
                'sales_amount' => 0 .' '
            );
        }

        if(!empty($sales)) {
            foreach($sales as $sale){
                $carbonDate = Carbon::createFromFormat('Y-m-d H:i:s', $sale->full_date);

                $daily_sale[$sale->date] = "<span class='text-warning'>". $sale->tax."</span><br>".$sale->discount."<br><span class='text-success'>".$sale->total."</span><br><span style='border-top:1px solid #DDD;'>".$sale->grand_total."</span>";
                $daily_sale2[$sale->date] = array(
                    'date' => $sale->date,
                    'day' => lang('cal_'. strtolower($carbonDate->format('l'))),
                    'sales' => $this->tec->formatMoney($sale->sale_total) .' ',
                    'sales_amount' => $this->tec->formatMoney($sale->grand_total) .' '
                );
            }
        } else {
            $daily_sale = array();
            $daily_sale2 = array();
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_sale);


        $store_name = lang('all_stores');
        $stores = $this->site->getAllStores();
        if ($store_id !== 'all') {
            $_store = $this->site->getStoreByID($store_id); //->{$st->name} 
            $store_name = $_store->name; // .' ('. $_store->code .')';
        }

    
        $start = $year.'-'.$month.'-01 00:00:00';
        $end = $year.'-'.$month.'-'.days_in_month($month, $year).' 23:59:59';
        $this->data['total_purchases'] = $this->reports_model->getTotalPurchases($store_id, $start, $end);
        $this->data['total_sales'] = $this->reports_model->getTotalSales($store_id, $start, $end);
        $this->data['total_expenses'] = $this->reports_model->getTotalExpenses($store_id, $start, $end);
        $this->data['page_title'] = $this->lang->line("daily_sales");
        $this->data['stores'] = $stores;
        $this->data['store_name'] = $store_name;
        $this->data['store_id'] = $store_id;
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        $bc = array(array('link' => '#', 'page' => lang('reports')), array('link' => '#', 'page' => lang('daily_sales')));
        $meta = array('page_title' => lang('daily_sales'), 'bc' => $bc);

        if ($report == 'print' || $report == 'excel') {
            $this->load->library('report');
            $this->report->setOptions(array(
                'template' => FCPATH . 'files/daily_reports.xlsx',
                'data' => array(
                    'inv' => array(
                        'row' => array(
                            'title' => lang('daily_sales'),

                            'txt_sales' => lang('sales_value'),
                            'total_sales_amount' => $this->tec->formatMoney($this->data['total_sales']->total_amount) .' ',
                            'total_sales' => $this->data['total_sales']->total .' ',
                            'txt_total_sales' => lang('sales'),
                            'total_paid' => $this->tec->formatMoney($this->data['total_sales']->paid) .' ',
                            'total_tax' => $this->tec->formatMoney($this->data['total_sales']->tax) .' ',

                            'txt_profit_loss' => lang('profit_loss'),
                            'total_profit' => $this->tec->formatMoney($this->data['total_sales']->total_amount - $this->data['total_purchases']->total_amount - $this->data['total_expenses']->total_amount) .' ',
                            'profit_detail' => $this->data['total_sales']->total_amount.' - '.($this->data['total_purchases']->total_amount ? $this->data['total_purchases']->total_amount : 0).' - '.($this->data['total_expenses']->total_amount ? $this->data['total_expenses']->total_amount : 0),
                        
                            'txt_total_purchases' => lang('purchases_value'),
                            'total_purchases_amount' => $this->tec->formatMoney($this->data['total_purchases']->total_amount) .' ',
                            'total_purchases' => $this->data['total_purchases']->total .' ',
                            'txt_total_purchases' => lang('purchases'),

                            'txt_expenses' => lang('expenses_value'),
                            'total_expenses_amount' => $this->tec->formatMoney($this->data['total_expenses']->total_amount) .' ',
                            'total_expenses' => $this->data['total_expenses']->total .' ',
                            'txt_total_expenses' => lang('expenses'),
                            
                            'txt_date' => $monthName .' '. $year,
                            'title_date' => lang('date'),
                            'title_day' => lang('day'),
                            'title_sales' => lang('sales'),
                            'title_sales_amount' => lang('total_sales'),

                            'txt_day_total' => lang('total')
                        )
                    ),
                    'dt' => array(
                        'rows' => $daily_sale2
                    )
                )
            ));
            echo $this->report->render($report);
        } else {
            $this->page_construct('reports/daily', $this->data, $meta);
        }
    }


    function monthly_sales($store_id = 'all', $year = NULL, $report = 'no')
    {
        
        $store_name = lang('all_stores');
        $stores = $this->site->getAllStores();
        if ($store_id !== 'all') {
            $_store = $this->site->getStoreByID($store_id); //->{$st->name} 
            $store_name = $_store->name; // .' ('. $_store->code .')';
        }

        $this->data['stores'] = $stores;
        $this->data['store_name'] = $store_name;
        $this->data['store_id'] = $store_id;
        $this->data['year'] = $year;

        if(!$year) { $year = date('Y'); }
        $this->load->language('calendar');
        $this->lang->load('calendar');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $start = $year.'-01-01 00:00:00';
        $end = $year.'-12-31 23:59:59';
        $this->data['total_purchases'] = $this->reports_model->getTotalPurchases($store_id, $start, $end);
        $this->data['total_sales'] = $this->reports_model->getTotalSales($store_id, $start, $end);
        $this->data['total_expenses'] = $this->reports_model->getTotalExpenses($store_id, $start, $end);
        $this->data['year'] = $year;
        $this->data['sales'] = $this->reports_model->getMonthlySales($store_id, $year);
        $this->data['page_title'] = $this->lang->line("monthly_sales");
        $bc = array(array('link' => '#', 'page' => lang('reports')), array('link' => '#', 'page' => lang('monthly_sales')));
        $meta = array('page_title' => lang('monthly_sales'), 'bc' => $bc);


        $monthly_sale = array();
        for($i = 1; $i <= 12; $i++) {
            $carbonDate = Carbon::createFromFormat('Y-m-d', $year .'-'. ($i < 10 ? '0'. $i : $i) .'-01');
            $monthly_sale[] = array(
                'date' => $i,
                'total' => '0 ',
                'grand_total' => '0 ',
                'tax' => '0 ',
                'discount' => '0 ',
                'sales' => '0 ',
                'month' => lang('cal_'. strtolower($carbonDate->format('F'))),
            );
        }

        foreach($this->data['sales'] as $sale) {
            $i = ((int) $sale->date);

            $carbonDate = Carbon::createFromFormat('Y-m-d', $year .'-'. ($i < 10 ? '0'. $i : $i) .'-01');
            $monthly_sale[ ((int) $sale->date) -1 ] = array(
                'date' => $sale->date,
                'total' => $this->tec->formatMoney($sale->total) .' ',
                'grand_total' => $this->tec->formatMoney($sale->grand_total) .' ',
                'tax' => $this->tec->formatMoney($sale->tax) .' ',
                'discount' => $this->tec->formatMoney($sale->discount) .' ',
                'sales' => $sale->sale_total .' ',
                'month' => lang('cal_'. strtolower($carbonDate->format('F'))),
            );
        }

        if ($report == 'print' || $report == 'excel') {
            $this->load->library('report');
            $this->report->setOptions(array(
                'template' => FCPATH . 'files/monthly_reports.xlsx',
                'data' => array(
                    'inv' => array(
                        'row' => array(
                            'title' => lang('monthly_sales'),

                            'txt_sales' => lang('sales_value'),
                            'total_sales_amount' => $this->tec->formatMoney($this->data['total_sales']->total_amount) .' ',
                            'total_sales' => $this->data['total_sales']->total .' ',
                            'txt_total_sales' => lang('sales'),
                            'total_paid' => '', // $this->tec->formatMoney($this->data['total_sales']->paid) .' ',
                            'total_tax' => '', // $this->tec->formatMoney($this->data['total_sales']->tax) .' ',

                            'txt_profit_loss' => lang('profit_loss'),
                            'total_profit' => $this->tec->formatMoney($this->data['total_sales']->total_amount - $this->data['total_purchases']->total_amount - $this->data['total_expenses']->total_amount) .' ',
                            'profit_detail' => $this->data['total_sales']->total_amount.' - '.($this->data['total_purchases']->total_amount ? $this->data['total_purchases']->total_amount : 0).' - '.($this->data['total_expenses']->total_amount ? $this->data['total_expenses']->total_amount : 0),
                        
                            'txt_total_purchases' => lang('purchases_value'),
                            'total_purchases_amount' => $this->tec->formatMoney($this->data['total_purchases']->total_amount) .' ',
                            'total_purchases' => $this->data['total_purchases']->total .' ',
                            'txt_total_purchases' => lang('purchases'),

                            'txt_expenses' => lang('expenses_value'),
                            'total_expenses_amount' => $this->tec->formatMoney($this->data['total_expenses']->total_amount) .' ',
                            'total_expenses' => $this->data['total_expenses']->total .' ',
                            'txt_total_expenses' => lang('expenses'),
                            
                            'txt_date' => $year,
                            'title_no' => lang('no'),
                            'title_month' => lang('month'),
                            'title_sales' => lang('sales'),
                            'title_sales_amount' => lang('total_sales'),

                            'txt_day_total' => lang('total')
                        )
                    ),
                    'dt' => array(
                        'rows' => $monthly_sale
                    )
                )
            ));
            echo $this->report->render($report);
        } else {
            $this->page_construct('reports/monthly', $this->data, $meta);
        }
    }

    function index($store_id = 'all')
    {
        if($this->input->get('customer')) {
            $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
            $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
            $user = $this->input->get('user') ? $this->input->get('user') : NULL;
            $this->data['total_sales'] = $this->reports_model->getTotalSalesforCustomer($this->input->get('customer'), $user, $start_date, $end_date);
            $this->data['total_sales_value'] = $this->reports_model->getTotalSalesValueforCustomer($this->input->get('customer'), $user, $start_date, $end_date);
        }
        
        $store_name = lang('all_stores');
        $stores = $this->site->getAllStores();
        if ($store_id !== 'all') {
            $_store = $this->site->getStoreByID($store_id); //->{$st->name} 
            $store_name = $_store->name; // .' ('. $_store->code .')';
        }
        $this->data['store_name'] = $store_name;
        $this->data['stores'] = $stores;
        $this->data['store_id'] = $store_id;
        
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['customers'] = $this->reports_model->getAllCustomers();
        $this->data['users'] = $this->reports_model->getAllStaff();
        $this->data['page_title'] = $this->lang->line("sales_report");
        $bc = array(array('link' => '#', 'page' => lang('reports')), array('link' => '#', 'page' => lang('sales_report')));
        $meta = array('page_title' => lang('sales_report'), 'bc' => $bc);
        $this->page_construct('reports/sales', $this->data, $meta);
    }

    function get_sales($store_id = null)
    {
        if ($this->input->get('v')) {
            $store_id = $this->input->get('v');
        }

        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;

        $this->load->library('datatables');
        $print = $this->datatables
        ->select("id as pid, date, customer_name, grand_total, paid, (grand_total-paid) as balance, status")
        ->from($this->db->dbprefix('sales'));

        
        $this->datatables->add_column("Actions", "<div class='text-center'><div class='btn-group'><a href='#' onClick=\"MyWindow=window.open('" . site_url('pos/view/$1/1') . "', 'MyWindow','toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600'); return false;\" title='".lang("view_invoice")."' class='tip btn btn-primary btn-xs'><i class='fa fa-list'></i></a><a href='".site_url('reports/view/$1')."' title='" . lang("view") . "' class='tip btn btn-primary btn-xs'><i class='fa fa-file-text-o'></i></a> <a href='" . site_url('reports/edit/$1') . "' title='" . lang("edit_product") . "' class='tip btn btn-warning btn-xs'><i class='fa fa-edit'></i></a> </div></div>", "pid, image, code, pname, barcode_symbology");
        $this->datatables->unset_column('pid')->unset_column('barcode_symbology');
        
        // if ($this->session->userdata('store_id')) {
            // $this->datatables->where('store_id', $this->session->userdata('store_id'));
        // }
        
        if ($store_id != 'all') {
            $this->datatables->where('store_id', $store_id);
        }
        
        $this->datatables->unset_column('id');
        if($customer) { $this->datatables->where('customer_id', $customer); }
        if($user) { $this->datatables->where('created_by', $user); }
        if($start_date) { $this->datatables->where('date >=', $start_date); }
        if($end_date) { $this->datatables->where('date <=', $end_date); }

        // $this->datatables->group_by('date(date)');

        echo $this->datatables->generate();
    }

    public function sales_export($type = 'print')
    {
        if ($this->input->get('v')) {
            $store_id = $this->input->get('v');
        } else {
            $store_id = 'all';
        }
        
        $namaToko = 'Semua Toko';

        if ($store_id != 'all') {
            $_store = $this->site->getStoreByID($store_id); //->{$st->name} 
            $namaToko = $_store->name; // .' ('. $_store->code .')';
        }

        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;

        $this->db->select("id as pid, date(date) tanggal, date, customer_name, grand_total, paid, (grand_total-paid) as balance, status");
        $this->db->from($this->db->dbprefix('sales'));

        if ($store_id != 'all') {
            $this->db->where('store_id', $store_id);
        }

        if($customer) { $this->db->where('customer_id', $customer); }
        if($user) { $this->db->where('created_by', $user); }
        if($start_date) { $this->db->where('date >=', $start_date); }
        if($end_date) { $this->db->where('date <=', $end_date); }

        $this->db->order_by('date', 'desc');
        
        $data = $this->db->get()->result_object();


        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Dida Nurwanda")
            ->setLastModifiedBy("Dida Nurwanda")
            ->setTitle("Laporan Penjualan $namaToko");
        $sheet = $objPHPExcel->setActiveSheetIndex(0);
        
        // title
        $sheet->setCellValue('B1', 'LAPORAN PENJUALAN '. strtoupper($namaToko));
        $sheet->getStyle('B1')->getFont()->setSize(13);
        $sheet->getStyle('B1')->getFont()->setBold(true);

        $sheet->getColumnDimension('A')->setWidth(15);
        $sheet->getColumnDimension('B')->setWidth(12);
        $sheet->getColumnDimension('C')->setWidth(25);

        // $sheet->mergeCells('C3:E3');

        $sheet->setCellValue('A3', 'Tanggal');
        $sheet->setCellValue('B3', 'No Transaksi');
        $sheet->setCellValue('C3', 'Menu');
        $sheet->setCellValue('D3', 'Jumlah');
        $sheet->setCellValue('E3', 'Harga');
        $sheet->setCellValue('F3', 'Sub Total');
        $sheet->setCellValue('G3', 'Total');
        $sheet->getStyle('A3')->getFont()->setBold(true);
        $sheet->getStyle('B3')->getFont()->setBold(true);
        $sheet->getStyle('C3')->getFont()->setBold(true);
        $sheet->getStyle('D3')->getFont()->setBold(true);
        $sheet->getStyle('E3')->getFont()->setBold(true);
        $sheet->getStyle('F3')->getFont()->setBold(true);
        $sheet->getStyle('G3')->getFont()->setBold(true);

        $cfgBorderAll = [
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ]
        ];
        
        $cfgBorderLeftRight = [
            'borders' => [
                'left' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ],
                'right' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000']
                ]
            ]
        ];

        $sheet->getStyle('A3:G3')->applyFromArray($cfgBorderAll);

        $start = 3;
        $lastTanggal = null;
        $noTransaksi = 1;
        
        foreach($data as $item) {
            
            $sheet->getStyle('A'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('B'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('C'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('D'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('E'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('F'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('G'. $start)->applyFromArray($cfgBorderLeftRight);
            
            $start++;

            $sheet->getStyle('A'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('B'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('C'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('D'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('E'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('F'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('G'. $start)->applyFromArray($cfgBorderLeftRight);
            
            if ($item->tanggal != $lastTanggal) {
                // $start++;
                $lastTanggal = $item->tanggal;
                $sheet->setCellValue('A'. $start, $lastTanggal);
                $noTransaksi = 1;
                // $sheet->getStyle('A'. $start)->getFont()->setBold(true);
                // $start++;
            }

            // $sheet->setCellValue('A'. $start, $item->date);
            $sheet->setCellValue('B'. $start, $noTransaksi);
            // $sheet->setCellValue('C'. $start, $item->grand_total);
            
            $detail = $this->reports_model->getSales($item->pid);
            // $noTransaksi = 1;
            foreach($detail as $item2) {
                $sheet->setCellValue('C'. $start, $item2['name_product']);
                $sheet->setCellValue('D'. $start, $item2['qty']);
                $sheet->setCellValue('E'. $start, $item2['unit_price']);
                $sheet->setCellValue('F'. $start, $item2['unit_price'] * $item2['qty']);
                
                $sheet->getStyle('A'. $start)->applyFromArray($cfgBorderLeftRight);
                $sheet->getStyle('B'. $start)->applyFromArray($cfgBorderLeftRight);
                $sheet->getStyle('C'. $start)->applyFromArray($cfgBorderAll);
                $sheet->getStyle('D'. $start)->applyFromArray($cfgBorderAll);
                $sheet->getStyle('E'. $start)->applyFromArray($cfgBorderAll);
                $sheet->getStyle('F'. $start)->applyFromArray($cfgBorderAll);
                $sheet->getStyle('G'. $start)->applyFromArray($cfgBorderAll);
                
                $start++;
            }
            // $sheet->setCellValue('D'. $start, 'Total');
            $sheet->setCellValue('G'. $start, $item->grand_total);
            
            $sheet->getStyle('A'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('B'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('C'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('D'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('E'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('F'. $start)->applyFromArray($cfgBorderLeftRight);
            $sheet->getStyle('G'. $start)->applyFromArray($cfgBorderAll);
            
            $start++;
            $noTransaksi++;
        }


        // Redirect output to a client’s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="laporan_harian.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
        
    }

    function view($id){
        $this->data['page_title'] = $this->lang->line("sales_report");
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['sales'] = $this->reports_model->getSales($id);
        //$this->data['sale'] = $this->reports_model->getSalesDetail($id);
        $bc = array(array('link' => '#', 'page' => lang('reports')), array('link' => '#', 'page' => lang('sales_report')));
        $meta = array('page_title' => lang('sales_report'), 'bc' => $bc);
        $this->page_construct('reports/view', $this->data, $meta);
    }

    function edit($id) {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['sales'] = $this->reports_model->getSales($id);
        //$this->data['sale'] = $this->reports_model->getSalesDetail($id);
        $bc = array(array('link' => '#', 'page' => lang('reports')), array('link' => site_url('reports'), 'page' => lang('sales_report')), array('link' => '#', 'page' => lang('edit')));
        $meta = array('page_title' => lang('sales_report'), 'bc' => $bc);
        $this->page_construct('reports/edit', $this->data, $meta);
    }
    function update_bayar($id) {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $paid = $this->input->post('paid');
        $status = $this->input->post('status');
        $data = array(
                    'paid' => $paid,
                    'status' => $status
                    );
        $where = array('id' => $id);

        $this->db->where($where);
        $this->db->update('sales',$data);
        //$this->data['sales'] = $this->reports_model->getSalesUpdate($id);
        //$this->data['sale'] = $this->reports_model->getSalesDetail($id);
        redirect('reports');
    }

    function products()
    {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['products'] = $this->reports_model->getAllProducts();
        $this->data['page_title'] = $this->lang->line("products_report");
        $this->data['page_title'] = $this->lang->line("products_report");
        $bc = array(array('link' => '#', 'page' => lang('reports')), array('link' => '#', 'page' => lang('products_report')));
        $meta = array('page_title' => lang('products_report'), 'bc' => $bc);
        $this->page_construct('reports/products', $this->data, $meta);
    }

    function get_products()
    {
        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        //COALESCE(sum(".$this->db->dbprefix('sale_items').".quantity)*".$this->db->dbprefix('products').".cost, 0) as cost,
        $this->load->library('datatables');
        $this->datatables
        ->select($this->db->dbprefix('products').".name, ".$this->db->dbprefix('products').".code, COALESCE(sum(".$this->db->dbprefix('sale_items').".quantity), 0) as sold, ROUND(COALESCE(((sum(".$this->db->dbprefix('sale_items').".subtotal)*".$this->db->dbprefix('products').".tax)/100), 0), 2) as tax, COALESCE(sum(".$this->db->dbprefix('sale_items').".quantity)*".$this->db->dbprefix('sale_items').".cost, 0) as cost, COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal), 0) as income,
            ROUND((COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal), 0)) - COALESCE(sum(".$this->db->dbprefix('sale_items').".quantity)*".$this->db->dbprefix('sale_items').".cost, 0) -COALESCE(((sum(".$this->db->dbprefix('sale_items').".subtotal)*".$this->db->dbprefix('products').".tax)/100), 0), 2)
            as profit", FALSE)
        ->from('sale_items')
        ->join('products', 'sale_items.product_id=products.id', 'left')
        ->join('sales', 'sale_items.sale_id=sales.id', 'left');
        if ($this->session->userdata('store_id')) {
            $this->datatables->where('sales.store_id', $this->session->userdata('store_id'));
        }
        $this->datatables->group_by('products.id');

        if($product) { $this->datatables->where('products.id', $product); }
        if($start_date) { $this->datatables->where('date >=', $start_date); }
        if($end_date) { $this->datatables->where('date <=', $end_date); }
        echo $this->datatables->generate();
    }

    function profit( $income, $cost, $tax)
    {
        return floatval($income)." - ".floatval($cost)." - ".floatval($tax);
    }

    function top_products($store_id = 'all')
    {
        $store_name = lang('all_stores');
        $stores = $this->site->getAllStores();
        if ($store_id !== 'all') {
            $_store = $this->site->getStoreByID($store_id); //->{$st->name} 
            $store_name = $_store->name; // .' ('. $_store->code .')';
        }

        $this->data['topProducts'] = $this->reports_model->topProducts($store_id);
        $this->data['topProducts1'] = $this->reports_model->topProducts1($store_id);
        $this->data['topProducts3'] = $this->reports_model->topProducts3($store_id);
        $this->data['topProducts12'] = $this->reports_model->topProducts12($store_id);
        $this->data['store_id'] = $store_id;
        $this->data['store_name'] = $store_name;
        $this->data['stores'] = $stores;
        $this->data['page_title'] = $this->lang->line("top_products");
        $bc = array(array('link' => '#', 'page' => lang('reports')), array('link' => '#', 'page' => lang('top_products')));
        $meta = array('page_title' => lang('top_products'), 'bc' => $bc);
        $this->page_construct('reports/top', $this->data, $meta);
    }

    function registers()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getAllStaff();
        $bc = array(array('link' => '#', 'page' => lang('reports')), array('link' => '#', 'page' => lang('registers_report')));
        $meta = array('page_title' => lang('registers_report'), 'bc' => $bc);
        $this->page_construct('reports/registers', $this->data, $meta);
    }

    function get_register_logs()
    {
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;

        $this->load->library('datatables');
        $this->datatables
        ->select("date, closed_at, CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name, '<br>', " . $this->db->dbprefix('users') . ".email) as user, cash_in_hand, CONCAT(total_cc_slips, ' (', total_cc_slips_submitted, ')') as cc_slips, CONCAT(total_cheques, ' (', total_cheques_submitted, ')') as total_cheques, CONCAT(total_cash, ' (', total_cash_submitted, ')') as total_cash, note", FALSE)
        ->from("registers")
        ->join('users', 'users.id=registers.user_id', 'left');

        if ($user) {
            $this->datatables->where('registers.user_id', $user);
        }
        if ($start_date) {
            $this->datatables->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
        }
        if ($this->session->userdata('store_id')) {
            $this->datatables->where('registers.store_id', $this->session->userdata('store_id'));
        }

        echo $this->datatables->generate();


    }

    function payments()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getAllStaff();
        $this->data['customers'] = $this->reports_model->getAllCustomers();
        $bc = array(array('link' => '#', 'page' => lang('reports')), array('link' => '#', 'page' => lang('payments_report')));
        $meta = array('page_title' => lang('payments_report'), 'bc' => $bc);
        $this->page_construct('reports/payments', $this->data, $meta);
    }

    function get_payments()
    {
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $ref = $this->input->get('payment_ref') ? $this->input->get('payment_ref') : NULL;
        $sale_id = $this->input->get('sale_no') ? $this->input->get('sale_no') : NULL;
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $paid_by = $this->input->get('paid_by') ? $this->input->get('paid_by') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;

        $this->load->library('datatables');
        $this->datatables
        ->select($this->db->dbprefix('payments') . ".date, " . $this->db->dbprefix('payments') . ".reference as ref, " . $this->db->dbprefix('sales') . ".id as sale_no, paid_by, amount")
        ->from('payments')
        ->join('sales', 'payments.sale_id=sales.id', 'left')
        ->group_by('payments.id');

        if ($this->session->userdata('store_id')) {
            $this->datatables->where('payments.store_id', $this->session->userdata('store_id'));
        }
        if ($user) {
            $this->datatables->where('payments.created_by', $user);
        }
        if ($ref) {
            $this->datatables->where('payments.reference', $ref);
        }
        if ($paid_by) {
            $this->datatables->where('payments.paid_by', $paid_by);
        }
        if ($sale_id) {
            $this->datatables->where('sales.id', $sale_id);
        }
        if ($customer) {
            $this->datatables->where('sales.customer_id', $customer);
        }
        if ($start_date) {
            $this->datatables->where($this->db->dbprefix('payments').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
        }

        echo $this->datatables->generate();

    }

    function alerts() {
        $data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['page_title'] = lang('stock_alert');
        $bc = array(array('link' => '#', 'page' => lang('stock_alert')));
        $meta = array('page_title' => lang('stock_alert'), 'bc' => $bc);
        $this->page_construct('reports/alerts', $this->data, $meta);

    }

    function get_alerts() {
        $this->load->library('datatables');
        $this->datatables->select($this->db->dbprefix('products').".id as pid, ".$this->db->dbprefix('products').".image as image, ".$this->db->dbprefix('products').".code as code, ".$this->db->dbprefix('products').".name as pname, ".$this->db->dbprefix('products').".type as type, ".$this->db->dbprefix('categories').".name as cname, (CASE WHEN psq.quantity IS NULL THEN 0 ELSE psq.quantity END) as quantity, ".$this->db->dbprefix('products').".alert_quantity as alert_quantity, ".$this->db->dbprefix('products').".tax as tax, ".$this->db->dbprefix('products').".tax_method as task_method, ".$this->db->dbprefix('products').".cost as cost, ".$this->db->dbprefix('products').".price as price", FALSE)
        ->from($this->db->dbprefix('products'))
        ->join($this->db->dbprefix('categories'), $this->db->dbprefix('categories') .'.id='. $this->db->dbprefix('products') .'.category_id')
        ->join("( SELECT * from {$this->db->dbprefix('product_store_qty')} WHERE store_id = {$this->session->userdata('store_id')}) psq", $this->db->dbprefix('products') .'.id=psq.product_id', 'left')
        ->where("(CASE WHEN psq.quantity IS NULL THEN 0 ELSE psq.quantity END) < {$this->db->dbprefix('products')}.alert_quantity", NULL, FALSE)
        ->group_by($this->db->dbprefix('products').'.id');

        $this->datatables->add_column("Actions", "<div class='text-center'><a href='#' class='btn btn-xs btn-primary ap tip' data-id='$1' title='".lang('add_to_purcahse_order')."'><i class='fa fa-plus'></i></a></div>", "pid");
        $this->datatables->unset_column('pid');
        echo $this->datatables->generate();
    }

    function tenan_product()
    {
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['tenans'] = $this->reports_model->getAllTenan();
        $this->data['page_title'] = $this->lang->line("tenan_report");
        $bc = array(array('link' => '#', 'page' => lang('reports')), array('link' => '#', 'page' => lang('tenan_report')));
        $meta = array('page_title' => lang('tenan_report'), 'bc' => $bc);
        $this->page_construct('reports/tenan', $this->data, $meta);
    }

    function get_tenan()
    {
        $tenan = $this->input->get('tenan') ? $this->input->get('tenan') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        //COALESCE(sum(".$this->db->dbprefix('sale_items').".quantity)*".$this->db->dbprefix('products').".cost, 0) as cost,
        $this->load->library('datatables');

        $sql = $this->datatables
        ->select($this->db->dbprefix('products').".name, ".$this->db->dbprefix('categories').".name as name_kategori, COALESCE(sum(".$this->db->dbprefix('sale_items').".quantity), 0) as sold, ".$this->db->dbprefix('sales').".date, COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal), 0) as income,
            COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal) * 0.8, 0) as cost,
            COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal) * 0.2, 0) as profit", FALSE)
        ->from('sale_items')
        ->join('products', 'sale_items.product_id=products.id', 'left')
        ->join('categories', 'categories.id=products.category_id', 'left')
        ->join('sales', 'sale_items.sale_id=sales.id', 'left');
        if ($this->session->userdata('store_id')) {
            $this->datatables->where('sales.store_id', $this->session->userdata('store_id'));
        }
        $sql->group_by('products.id');

        if($tenan) { $sql->where('categories.id', $tenan); }
        if($start_date) { $sql->where('date >=', $start_date); }
        if($end_date) { $sql->where('date <=', $end_date); }
        echo $sql->generate();

    }

    function get_tenan_sum()
    {
        $tenan = $this->input->get('tenan') ? $this->input->get('tenan') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        //COALESCE(sum(".$this->db->dbprefix('sale_items').".quantity)*".$this->db->dbprefix('products').".cost, 0) as cost,
        $this->load->library('datatables3');
        $sql = $this->datatables3
        ->select($this->db->dbprefix('sales').".date, ".$this->db->dbprefix('categories').".name as name_kategori, COALESCE(sum(".$this->db->dbprefix('sale_items').".quantity), 0) as sold, COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal), 0) as income,
            COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal) * 0.8, 0) as cost,
            COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal) * 0.2, 0) as profit", FALSE)
        ->from('sale_items')
        ->join('products', 'sale_items.product_id=products.id', 'left')
        ->join('categories', 'categories.id=products.category_id', 'left')
        ->join('sales', 'sale_items.sale_id=sales.id', 'left');
        if ($this->session->userdata('store_id')) {
            $this->datatables3->where('sales.store_id', $this->session->userdata('store_id'));
        }
        $sql->group_by('categories.id');

        if($tenan) { $sql->where('categories.id', $tenan); }
        if($start_date) { $sql->where('date >=', $start_date); }
        if($end_date) { $sql->where('date <=', $end_date); }
        echo $sql->generate();

    }

    function get_tenan_sum_tot()
    {
        $tenan = $this->input->get('tenan') ? $this->input->get('tenan') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        //COALESCE(sum(".$this->db->dbprefix('sale_items').".quantity)*".$this->db->dbprefix('products').".cost, 0) as cost,
        $this->load->library('datatables3'); 
        $sql = $this->datatables3
        ->select("COALESCE(sum(".$this->db->dbprefix('sale_items').".quantity), 0) as sold, COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal), 0) as income,
            COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal) * 0.8, 0) as cost,
            COALESCE(sum(".$this->db->dbprefix('sale_items').".subtotal) * 0.2, 0) as profit", FALSE)
        
        ->from('sale_items')
        ->join('products', 'sale_items.product_id=products.id', 'left')
        ->join('categories', 'categories.id=products.category_id', 'left')
        ->join('sales', 'sale_items.sale_id=sales.id', 'left');
        if ($this->session->userdata('store_id')) {
            $this->datatables3->where('sales.store_id', $this->session->userdata('store_id'));
        }

        if($tenan) { $sql->where('categories.id', $tenan); }
        if($start_date) { $sql->where('date >=', $start_date); }
        if($end_date) { $sql->where('date <=', $end_date); }
        echo $sql->generate();

    }

    function excel_report(){
        $this->data['tampil'] = $this->reports_model->excel_report();
        //$this->data['sale'] = $this->reports_model->getSalesDetail($id);
        print_r($tampil);die();
        $this->page_construct('reports/excel', $this->data);
    }

}
