<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CI_Model
{


	public function __construct()
	{
		parent::__construct();

	}

	public function getAllProducts()
	{
		$q = $this->db->get('products');
		if($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}
        return FALSE;
	}

	public function getAllCustomers()
	{
		$q = $this->db->get('customers');
		if($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}
        return FALSE;
	}

	public function topProducts($store_id = 'all')
	{
		$m = date('Y-m');
		$this->db->select($this->db->dbprefix('products').".code as product_code, ".$this->db->dbprefix('products').".name as product_name, sum(".$this->db->dbprefix('sale_items').".quantity) as quantity")
		->join('products', 'products.id=sale_items.product_id', 'left')
		->join('sales', 'sales.id=sale_items.sale_id', 'left')
		->order_by("sum(".$this->db->dbprefix('sale_items').".quantity)", 'desc')
		->group_by('sale_items.product_id')
		->limit(10)
		->like('sales.date', $m, 'both');
		
		if ($store_id !== 'all') {
            $this->db->where('store_id', $store_id);
		}

		$q = $this->db->get('sale_items');
		if($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}
        return FALSE;
	}

	public function topProducts1($store_id = 'all')
	{
		$m = date('Y-m', strtotime('first day of last month'));
		$this->db->select($this->db->dbprefix('products').".code as product_code, ".$this->db->dbprefix('products').".name as product_name, sum(".$this->db->dbprefix('sale_items').".quantity) as quantity")
		->join('products', 'products.id=sale_items.product_id', 'left')
		->join('sales', 'sales.id=sale_items.sale_id', 'left')
		->order_by("sum(".$this->db->dbprefix('sale_items').".quantity)", 'desc')
		->group_by('sale_items.product_id')
		->limit(10)
		->like('sales.date', $m, 'both');
		
		if ($store_id !== 'all') {
            $this->db->where('store_id', $store_id);
		}
		$q = $this->db->get('sale_items');
		if($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}
        return FALSE;
	}

	public function topProducts3($store_id = 'all')
	{
		$this->db->select($this->db->dbprefix('products').".code as product_code, ".$this->db->dbprefix('products').".name as product_name, sum(".$this->db->dbprefix('sale_items').".quantity) as quantity")
		->join('products', 'products.id=sale_items.product_id', 'left')
		->join('sales', 'sales.id=sale_items.sale_id', 'left')
		->order_by("sum(".$this->db->dbprefix('sale_items').".quantity)", 'desc')
		->group_by('sale_items.product_id')
		->limit(10)
		->where($this->db->dbprefix('sales').'.date >= last_day(now()) + interval 1 day - interval 3 month', NULL, FALSE);
		
		if ($store_id !== 'all') {
            $this->db->where('store_id', $store_id);
		}

		$q = $this->db->get('sale_items');
		if($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}
        return FALSE;
	}

	public function topProducts12($store_id = 'all')
	{
		$this->db->select($this->db->dbprefix('products').".code as product_code, ".$this->db->dbprefix('products').".name as product_name, sum(".$this->db->dbprefix('sale_items').".quantity) as quantity")
		->join('products', 'products.id=sale_items.product_id', 'left')
		->join('sales', 'sales.id=sale_items.sale_id', 'left')
		->order_by("sum(".$this->db->dbprefix('sale_items').".quantity)", 'desc')
		->group_by('sale_items.product_id')
		->limit(10)
		->where($this->db->dbprefix('sales').'.date >= last_day(now()) + interval 1 day - interval 12 month', NULL, FALSE);
		
		if ($store_id !== 'all') {
            $this->db->where('store_id', $store_id);
		}

		$q = $this->db->get('sale_items');
		if($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}
	}

	public function getDailySales($store_id, $year, $month)
	{
        $this->db->select("count(id) as sale_total, date as full_date, DATE_FORMAT( date,  '%e' ) AS date, COALESCE(sum(total), 0) as total, COALESCE(sum(grand_total), 0) as grand_total, COALESCE(sum(total_tax), 0) as tax, COALESCE(sum(total_discount), 0) as discount", FALSE)
        ->like('date', "{$year}-{$month}", 'after')
        ->group_by("DATE_FORMAT( date,  '%e' )");
		
		if ($store_id !== 'all') {
            $this->db->where('store_id', $store_id);
		}
		
		$q = $this->db->get('sales');
		if($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}


	public function getMonthlySales($store_id, $year)
	{
        $this->db->select("count(id) as sale_total, DATE_FORMAT( date,  '%c' ) AS date, COALESCE(sum(total), 0) as total, COALESCE(sum(grand_total), 0) as grand_total, COALESCE(sum(total_tax), 0) as tax, COALESCE(sum(total_discount), 0) as discount", FALSE)
        ->like('date', "{$year}", 'after')
        ->group_by("DATE_FORMAT( date,  '%c' )")
        ->order_by("DATE_FORMAT( date,  '%c' ) ASC");
		
		if ($store_id !== 'all') {
            $this->db->where('store_id', $store_id);
		}

        $q = $this->db->get('sales');
		if($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return FALSE;
	}

	public function getTotalSalesforCustomer($customer_id, $user = NULL, $start_date = NULL, $end_date = NULL)
	{
		if ($start_date && $end_date) {
			$this->db->where('date >=', $start_date);
			$this->db->where('date <=', $end_date);
		}
		if ($user) {
			$this->db->where('created_by', $user);
		}
        if ($this->session->userdata('store_id')) {
            $this->db->where('store_id', $this->session->userdata('store_id'));
        }
        $q=$this->db->get_where('sales', array('customer_id' => $customer_id));
        return $q->num_rows();
    }

    public function getTotalSalesValueforCustomer($customer_id, $user = NULL, $start_date = NULL, $end_date = NULL)
    {
        $this->db->select('sum(grand_total) as total');
        if($start_date && $end_date) {
            $this->db->where('date >=', $start_date);
            $this->db->where('date <=', $end_date);
        }
        if($user) {
            $this->db->where('created_by', $user);
        }
        if ($this->session->userdata('store_id')) {
            $this->db->where('store_id', $this->session->userdata('store_id'));
        }
        $q=$this->db->get_where('sales', array('customer_id' => $customer_id));
        if( $q->num_rows() > 0 ) {
            $s = $q->row();
            return $s->total;
        }
        return FALSE;
    }

	public function getAllStaff()
    {

        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

	public function getTotalSales($store_id, $start, $end)
    {
        $this->db->select('count(id) as total, sum(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid, SUM(COALESCE(total_tax, 0)) as tax', FALSE)
			->where("date >= '{$start}' and date <= '{$end}'", NULL, FALSE);
		
		if ($store_id != 'all') {
            $this->db->where('store_id', $store_id);
		}

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalPurchases($store_id, $start, $end)
    {
        $this->db->select('count(id) as total, sum(COALESCE(total, 0)) as total_amount', FALSE)
            ->where("date >= '{$start}' and date <= '{$end}'", NULL, FALSE);
		
		if ($store_id != 'all') {
            $this->db->where('store_id', $store_id);
		}
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTotalExpenses($store_id, $start, $end)
    {
        $this->db->select('count(id) as total, sum(COALESCE(amount, 0)) as total_amount', FALSE)
            ->where("date >= '{$start}' and date <= '{$end}'", NULL, FALSE);
		
		if ($store_id != 'all') {
            $this->db->where('store_id', $store_id);
		}
        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllTenan()
	{
		$q = $this->db->get('categories');
		if($q->num_rows() > 0) {
			foreach (($q->result()) as $row) {
				$data[] = $row;
			}
			return $data;
		}
        return FALSE;
	}

	function getSales($id){
		$this->db->select('sales.id, sales.customer_name, sales.grand_total, sales.paid, sales.total_items, sale_items.quantity as qty, sale_items.unit_price,
			products.name as name_product')
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->join('products', 'sale_items.product_id=products.id', 'left')
            ->order_by('sales.id', 'asc');
        $q = $this->db->get_where('sales', array('sales.id' => $id));

        return $q->result_array();
		
	}

	function getSalesUpdate($where,$data){
		$this->db->where($where);
		$this->db->update('sales',$data);
 		//return true;
		
	}

	function excel_report()
	{
		$start_date = $this->input->post('start_date');
		$end_date = $this->input->post('end_date');

		$this->db->select('sales.date, sales.sum(grand_total) as tot')
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->join('products', 'sale_items.product_id=products.id', 'left')
            ->join('categories', 'products.category_id=categories.id', 'left')
            ->order_by('sales.date', 'asc');
        $q = $this->db->get_where('sales', array('sales.date >=' => $start_date, 'sales.date <=' => $end_date));

        return $q->result_array();

	}


}
