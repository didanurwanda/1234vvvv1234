<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Excel_model extends CI_Model
{


	public function __construct()
	{
		parent::__construct();

	}

	function excel_report()
	{
		$start_date = $this->input->post('start_date'); $start = "$start_date 00:00:01";
		$end_date = $this->input->post('end_date'); $end = "$end_date 23:59:59";
		$this->db->select('sales.id, sales.date, sales.grand_total')
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->join('products', 'sale_items.product_id=products.id', 'left')
            ->join('categories', 'products.category_id=categories.id', 'left')
            ->order_by('sales.id', 'asc')
            ->where('sales.date >=', $start)
            ->where('sales.date <=', $end)
            ->group_by('sales.date');
        $q = $this->db->get('sales');

        return $q->result_array();

	}


}
