<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    public function topProducts($user_id = NULL, $store_id = 'all')
    {
        $isAdmin = $this->ion_auth->is_admin();
        $isOwner = $this->ion_auth->is_owner();

        $m = date('Y-m');
        // if(!$this->Admin) {
        //     $user_id = $this->session->userdata('user_id');
        // }
        $this->db->select($this->db->dbprefix('products').".code as product_code, ".$this->db->dbprefix('products').".name as product_name, sum(".$this->db->dbprefix('sale_items').".quantity) as quantity")
        ->join('products', 'products.id=sale_items.product_id', 'left')
        ->join('sales', 'sales.id=sale_items.sale_id', 'left')
        ->order_by("sum(".$this->db->dbprefix('sale_items').".quantity)", 'desc')
        ->group_by('sale_items.product_id')
        ->limit(10)
        ->like('sales.date', $m, 'both');

        if ($isAdmin || $isOwner) {

        } elseif ($this->session->userdata('store_id')) {
            $this->db->where('store_id', $this->session->userdata('store_id'));
        }

        // if($user_id) {
        //     $this->db->where('created_by', $user_id);
        // }

        $q = $this->db->get('sale_items');
        if($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getChartData($user_id = NULL) {
        $isAdmin = $this->ion_auth->is_admin();
        $isOwner = $this->ion_auth->is_owner();

        // if(!$this->Admin) {
        //     $user_id = $this->session->userdata('user_id');
        // }
        $myQuery = "SELECT S.month, S.total, S.tax, S.discount
            FROM (	SELECT	date_format(date, '%Y-%m') Month, SUM(total) total, SUM(total_tax) tax, SUM(total_discount) discount
                FROM ".$this->db->dbprefix('sales')."
                WHERE ".$this->db->dbprefix('sales').".date >= date_sub( now( ) , INTERVAL 12 MONTH ) ";
        // if($user_id) {
        //     $myQuery .= " AND created_by = ".$user_id." ";
        // }
        if ($isAdmin || $isOwner) {
            
        } elseif ($this->session->userdata('store_id')) {
            $myQuery .= " AND store_id = ".$this->session->userdata('store_id')." ";
        }
        
		$myQuery .= "GROUP BY date_format(date, '%Y-%m')) S
					GROUP BY S.Month
					ORDER BY S.Month";
        $q = $this->db->query($myQuery);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUserGroups() {
        $this->db->order_by('id', 'desc');
        $q = $this->db->get("users_groups");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function userGroups() {
        $ugs = $this->getUserGroups();
        if ($ugs) {
            foreach ($ugs as $ug) {
                $this->db->update('users', array('group_id' => $ug->group_id), array('id' => $ug->user_id));
            }
            return true;
        }
        return false;
    }

    public function getAllProducts() {
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function syncStoreQty() {
        $products = $this->getAllProducts();
        foreach ($products as $product) {
            $this->db->insert('product_store_qty', array('product_id' => $product->id, 'store_id' => 1, 'quantity' => $product->quantity));
        }
        $this->db->update('settings', array('version' => '4.0.5'), array('setting_id' => 1));
    }

}
