<?php if ($Settings->multi_store && !$this->session->userdata('has_store_id')) { ?>
<li id="mm_stores" class="mm_stores"><a href="<?= site_url('stores'); ?>"><i class="fa fa-building-o"></i> <span><?= lang('stores'); ?></span></a></li>
<?php } ?>
<li id="mm_pos" class="mm_pos"><a href="<?= site_url('pos'); ?>"><i class="fa fa-th"></i> <span><?= lang('pos'); ?></span></a></li>

<li id="mm_products" class="mm_products"><a href="<?= site_url('products'); ?>"><i class="fa fa-barcode"></i> <span><?= lang('products'); ?></span></a></li>
<?php if ($this->session->userdata('store_id')) { ?>
<li id="mm_sales" class="treeview mm_sales">
    <a href="#">
        <i class="fa fa-shopping-cart"></i>
        <span><?= lang('sales'); ?></span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li id="sales_index"><a href="<?= site_url('sales'); ?>"><i class="fa fa-circle-o"></i> <?= lang('list_sales'); ?></a></li>
        <li id="sales_opened"><a href="<?= site_url('sales/opened'); ?>"><i class="fa fa-circle-o"></i> <?= lang('list_opened_bills'); ?></a></li>
    </ul>
</li>
<?php } ?>
<li id="mm_gift_cards" class="treeview mm_gift_cards">
    <a href="#">
        <i class="fa fa-credit-card"></i>
        <span><?= lang('gift_cards'); ?></span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li id="gift_cards_index"><a href="<?= site_url('gift_cards'); ?>"><i class="fa fa-circle-o"></i> <?= lang('list_gift_cards'); ?></a></li>
    </ul>
</li>
<li id="mm_customers" class="treeview mm_customers">
    <a href="#">
        <i class="fa fa-users"></i>
        <span><?= lang('customers'); ?></span>
        <i class="fa fa-angle-left pull-right"></i>
    </a>
    <ul class="treeview-menu">
        <li id="customers_index"><a href="<?= site_url('customers'); ?>"><i class="fa fa-circle-o"></i> <?= lang('list_customers'); ?></a></li>
        <li id="customers_add"><a href="<?= site_url('customers/add'); ?>"><i class="fa fa-circle-o"></i> <?= lang('add_customer'); ?></a></li>
    </ul>
</li>