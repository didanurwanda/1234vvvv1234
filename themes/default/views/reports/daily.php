<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <div class="dropdown pull-right">
                        <?php if (!$this->session->userdata('has_store_id')) { ?>
                        <span>
                            <button class="btn btn-primary" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?= $store_name ?>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" id="dropdown" aria-labelledby="dLabel">
                                <?php if ($store_id !== null): ?>
                                <li><a href="<?= site_url('reports/daily_sales/all/'. $year .'/'. $month) ?>">Semua Toko</a></li>
                                <?php endif; ?>
                                <?php
                                foreach ($stores as $st) {
                                    if ($store_id != $st->id) {
                                        echo "<li><a href='".site_url('reports/daily_sales/'. $st->id .'/'. $year .'/'. $month)."'>{$st->name} ({$st->code})</a></li>";
                                    }
                                }
                                ?>
                            </ul>
                        </span>
                        <?php } ?>
                        <span>
                            <button class="btn btn-info" id="dPrint" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-share-square-o"></i>
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" id="dropdown2" aria-labelledby="dPrint">
                                <li><a target="_blank" href="<?= site_url('reports/daily_sales/'.  $store_id .'/'. $year .'/'. $month .'/print') ?>"><i class="fa fa-print"></i> Print</a></li>
                                <li><a href="<?= site_url('reports/daily_sales/'.  $store_id .'/'. $year .'/'. $month .'/excel') ?>"><i class="fa fa-file-excel-o"></i> Excel</a></li>
                            </ul>
                        </span>
                    </div>

                    
                    <h3 class="box-title">
                        <span class='text-warning'>
                            <?=$this->lang->line('tax')?> = <?= lang('orange'); ?></span>, <?=$this->lang->line('discount')?> = <?= lang('grey'); ?> &amp; <span class='text-success'><?=$this->lang->line('total')?> = <?= lang('green'); ?>
                            &nbsp; (<?= $store_name ?>)
                        </span>
                    </h3>
                </div>
                <div class="box-body">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box bg-aqua">
                                    <span class="info-box-icon"><i class="fa fa-shopping-cart"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><?= lang('sales_value'); ?></span>
                                        <span class="info-box-number"><?= $this->tec->formatMoney($total_sales->total_amount) ?></span>
                                        <div class="progress">
                                            <div style="width: 100%" class="progress-bar"></div>
                                        </div>
                                        <span class="progress-description">
                                            <?= $total_sales->total .' ' . lang('sales'); ?> |
                                            <?= $this->tec->formatMoney($total_sales->paid) . ' ' . lang('received') ?> |
                                            <?= $this->tec->formatMoney($total_sales->tax) . ' ' . lang('tax') ?>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box bg-yellow">
                                    <span class="info-box-icon"><i class="fa fa-plus"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><?= lang('purchases_value'); ?></span>
                                        <span class="info-box-number"><?= $this->tec->formatMoney($total_purchases->total_amount) ?></span>
                                        <div class="progress">
                                            <div style="width: 0%" class="progress-bar"></div>
                                        </div>
                                        <span class="progress-description">
                                            <?= $total_purchases->total ?> <?= lang('purchases'); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box bg-red">
                                    <span class="info-box-icon"><i class="fa fa-circle-o"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><?= lang('expenses_value'); ?></span>
                                        <span class="info-box-number"><?= $this->tec->formatMoney($total_expenses->total_amount) ?></span>
                                        <div class="progress">
                                            <div style="width: 0%" class="progress-bar"></div>
                                        </div>
                                        <span class="progress-description">
                                            <?= $total_expenses->total ?> <?= lang('expenses'); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3 col-sm-6 col-xs-12">
                                <div class="info-box bg-green">
                                    <span class="info-box-icon"><i class="fa fa-dollar"></i></span>
                                    <div class="info-box-content">
                                        <span class="info-box-text"><?= lang('profit_loss'); ?></span>
                                        <span class="info-box-number"><?= $this->tec->formatMoney($total_sales->total_amount-$total_purchases->total_amount-$total_expenses->total_amount) ?></span>
                                        <div class="progress">
                                            <div style="width: 100%" class="progress-bar"></div>
                                        </div>
                                        <span class="progress-description">
                                            <?= $total_sales->total_amount.' - '.($total_purchases->total_amount ? $total_purchases->total_amount : 0).' - '.($total_expenses->total_amount ? $total_expenses->total_amount : 0); ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="calendar table-responsive">
                            <?=$calender?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
