<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); 
$start_date = $this->input->post('start_date');
$end_date = $this->input->post('end_date');
?>
<div class="topadminmenu">
	<div class="tittle">
		<div align="center">
			<span class="style3">
				REKAPITULASI TRANSAKSI KANTIN
			</span><br>
			Periode <?php echo tgl_indo($start_date); ?> - <?php echo tgl_indo($end_date); ?>
		</div>
	</div>
</div>
<div align="center">
	<table width="53%" border="1" cellpadding="0" cellspacing="0">
		<tr bgcolor="#B8C5E4">
			<td height="45" colspan="3" align="center" valign="top"><span class="style2">ANEKA MINUMAN PESONA</span></td>
		</tr>
		<tr>
			<td width="30%" height="20"><div align="center"><strong>TANGGAL</strong></div></td>
			<td width="35%" height="20"><div align="center"><strong>PENDAPATAN</strong></div></td>
			<td width="20%" height="20"><div align="center"><strong>HARGA</strong></div></td>
		</tr>
		<?php
			foreach($tampil as $row){
		?>
		<tr>
			<td height="20"><div align="center"><?php echo $row['date']; ?></div></td>
			<td height="20"><span style="margin-left:10px;">Minuman</span></td>
			<td height="20"><span style="margin-left:40px;">Rp. <?php echo number_format ($row['grand_total'],0,".",","); ?></span></td>
		</tr>
		<?php } ?>
	</table>
	<br />
</div>
<br>