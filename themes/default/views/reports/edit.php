<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<br>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Update Pembayaran Utang</h3>
                </div>
                <div class="box-body">
                    <div class="col-lg-12">
                        <?= form_open_multipart("reports/update_bayar/".$sales['0']['id'], 'class="validation"');?>
                        <div class="row">
                            <div class="col-md-6">
                                <?php 
                                    $a = $sales['0']['paid']; $b = $sales['0']['grand_total'];
                                    if ($a == 0 ){$utang = $b;}else{$utang = '0';}
                                 ?>
                                <div class="form-group">
                                    Total Utang
                                    <?php echo form_input(array('name'=>'grand_total','class'=>'form-control','value'=>$utang,'readonly'=>'disable')); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    Bayar
                                    <?php echo form_input(array('name'=>'paid','class'=>'form-control','value'=>$utang)); ?>
                                    <?php echo form_input(array('name'=>'status','type'=>'hidden','value'=>'paid')); ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                        <div class="form-group">
                            <?php if ($a == 0){ echo 
                                '<a href="'.site_url().'reports">
                                    '.form_button('kembali', lang('Kembali'), 'class="btn btn-primary"').'
                                </a>
                                '.form_submit('edit_utang', lang('Bayar'), 'class="btn btn-primary"').'';
                            } else{ echo
                                '<a href="'.site_url().'reports">
                                    '.form_button('kembali', lang('Kembali'), 'class="btn btn-primary"').'
                                </a>';
                            }
                            ?>
                        </div>
                            </div>
                        </div>
                        <?= form_close();?>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<?= $assets ?>dist/js/jquery-ui.min.js" type="text/javascript"></script>