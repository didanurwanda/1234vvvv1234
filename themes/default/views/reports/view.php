<?php (defined('BASEPATH')) OR exit('No direct script access allowed'); ?>
<br>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Rincian Total Pembelian</h3>
                </div>
                <div class="box-body">
                    <div class="col-lg-12">
                        <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-borderless">
                                        <tbody>
                                            <tr>
                                                <td><b><?= lang("Name"); ?></b></td>
                                                <td><b><?php echo strtoupper($sales['0']['customer_name']);?></b></td>
                                            </tr>
                                            <tr>
                                                <td><b><?= lang("Total Hutang"); ?></b></td>
                                                <?php 
                                                    $a = $sales['0']['paid']; $b = $sales['0']['grand_total']; 
                                                 ?>
                                                <td>
                                                    <b><?php if ($a == 0 ){echo $b;}else{echo '0';} ?></b>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table class="table table-borderless table-striped dfTable table-right-left">
                                        <tbody>
                                            <tr>
                                                <td><u><?= lang("Detail Pembelian"); ?></u></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <?php 
                                                foreach($sales as $row) {
                                             ?>
                                            <tr>
                                                <td>
                                                    <?php echo $row['name_product']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['qty']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['unit_price']; ?>
                                                </td>
                                            </tr>
                                            <?php } ?>
                                            <tr style="background-color:#c7c7c7">
                                                <td>
                                                    <?= lang("grand_total"); ?>
                                                </td>
                                                <td>
                                                    <?php echo $sales['0']['total_items']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $sales['0']['grand_total']; ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                            </div>
                            <div class="col-md-6">
                                <a href="<?php echo site_url();?>reports"><?= form_button('kembali', lang('Kembali'), 'class="btn btn-primary"'); ?></a>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<?= $assets ?>dist/js/jquery-ui.min.js" type="text/javascript"></script>
