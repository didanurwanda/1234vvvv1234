<?php
function product_name($name)
{
    return character_limiter($name, (isset($Settings->char_per_line) ? ($Settings->char_per_line-8) : 35));
}

if ($modal) {
    echo '<div class="modal-dialog no-modal-header"><div class="modal-content"><div class="modal-body"><button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>';
} else { ?>
    <!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?= $page_title . " " . lang("no") . " " . $inv->id; ?></title>
        <base href="<?= base_url() ?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
        <link href="<?= $assets ?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .table { border-radius: 3px; }
            .table th { background: #f5f5f5; }
            .table th, .table td { vertical-align: middle !important; }
            h3 { margin: 5px 0; }

            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0; }
            }
        </style>
    </head>
    <body>

<?php } ?>
<div id="wrapper">
    <div id="receiptData" style="width: auto; max-width: 480px; min-width: 250px; margin: 0 auto;">
    <div class="no-print">
        <?php if ($message) { ?>
            <div class="alert alert-success">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= is_array($message) ? print_r($message, true) : $message; ?>
            </div>
        <?php } ?>
    </div>
    <div id="receipt-data">
        <div style="text-align:center;">
                <?= $Settings->header; ?>
                <?php
                if ($store) {
                    echo '<p style="text-align:center;">';
                    echo '<strong>'.$store->name.' ('.$store->code.')</strong><br>';
                    echo $store->city.'<br>'.$store->phone;
                    echo '</p>';
                }
                ?>
                <p style="text-align:center;">
                <?= lang('sale').' '.lang('number').': '.$inv->id; ?><br>
                <?= lang("customer").': '. $inv->customer_name; ?> <br>
                <?= lang("date").': '.$this->tec->hrld($inv->date); ?>
                </p>
            <div style="clear:both;"></div>
            <table class="table table-striped table-condensed">
                <thead>
                    <tr>
                        <th style="text-align:center; width: 50%; border-bottom: 2px solid #ddd;"><?=lang('description');?></th>
                        <th style="text-align:center; width: 12%; border-bottom: 2px solid #ddd;"><?=lang('quantity');?></th>
                        <th style="text-align:center; width: 24%; border-bottom: 2px solid #ddd;"><?=lang('price');?></th>
                        <th style="text-align:center; width: 26%; border-bottom: 2px solid #ddd;"><?=lang('subtotal');?></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $tax_summary = array();
                foreach ($rows as $row) {
                    echo '<tr><td style="text-align:left;">' . product_name($row->product_name) . '</td>';
                    echo '<td style="text-align:center;">' . $this->tec->formatNumber($row->quantity) . '</td>';
                    echo '<td style="text-align:right;">';

                    if ($inv->total_discount != 0) {
                        $price_with_discount = $this->tec->formatMoney($row->net_unit_price + $this->tec->formatDecimal($row->item_discount / $row->quantity));
                        $pr_tax = $row->tax_method ?
                        $this->tec->formatDecimal((($price_with_discount) * $row->tax) / 100) :
                        $this->tec->formatDecimal((($price_with_discount) * $row->tax) / (100 + $row->tax));
                        echo '<del>' . $this->tec->formatMoney($price_with_discount+$pr_tax) . '</del> ';
                    }

                    echo $this->tec->formatMoney($row->net_unit_price + ($row->item_tax / $row->quantity)) . '</td><td style="text-align:right;">' . $this->tec->formatMoney($row->subtotal) . '</td></tr>';
                }
                ?>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="2" style="text-align:left;"><?= lang("total"); ?></th>
                    <th colspan="2" style="text-align:right;"><?= $this->tec->formatMoney($inv->total + $inv->product_tax); ?></th>
                </tr>
                <?php
                if ($inv->order_tax != 0) {
                    echo '<tr><th colspan="2" style="text-align:left;">' . lang("order_tax") . '</th><th colspan="2" style="text-align:right;">' . $this->tec->formatMoney($inv->order_tax) . '</th></tr>';
                }
                if ($inv->total_discount != 0) {
                    echo '<tr><th colspan="2" style="text-align:left;">' . lang("order_discount") . '</th><th colspan="2" style="text-align:right;">' . $this->tec->formatMoney($inv->total_discount) . '</th></tr>';
                }

                if ($Settings->rounding) {
                    $round_total = $this->tec->roundNumber($inv->grand_total, $Settings->rounding);
                    $rounding = $this->tec->formatMoney($round_total - $inv->grand_total);
                ?>
                    <tr>
                        <th colspan="2" style="text-align:left;"><?= lang("rounding"); ?></th>
                        <th colspan="2" style="text-align:right;"><?= $rounding; ?></th>
                    </tr>
                    <tr>
                        <th colspan="2" style="text-align:left;"><?= lang("grand_total"); ?></th>
                        <th colspan="2" style="text-align:right;"><?= $this->tec->formatMoney($inv->grand_total + $rounding); ?></th>
                    </tr>
                <?php
                } else {
                    $round_total = $inv->grand_total;
                    ?>
                    <tr>
                        <th colspan="2" style="text-align:left;"><?= lang("grand_total"); ?></th>
                        <th colspan="2" style="text-align:right;"><?= $this->tec->formatMoney($inv->grand_total); ?></th>
                    </tr>
                <?php }
                if ($inv->paid < $round_total) { ?>
                    <tr>
                        <th colspan="2" style="text-align:left;"><?= lang("paid_amount"); ?></th>
                        <th colspan="2" style="text-align:right;"><?= $this->tec->formatMoney($inv->paid); ?></th>
                    </tr>
                    <tr>
                        <th colspan="2" style="text-align:left;"><?= lang("due_amount"); ?></th>
                        <th colspan="2" style="text-align:right;"><?= $this->tec->formatMoney($inv->grand_total - $inv->paid); ?></th>
                    </tr>
                <?php } ?>
                </tfoot>
            </table>
            <?php
            if ($payments) {
                echo '<table class="table table-striped table-condensed" style="margin-top:10px;"><tbody>';
                foreach ($payments as $payment) {
                    echo '<tr>';
                    if ($payment->paid_by == 'cash' && $payment->pos_paid) {
                        echo '<td style="padding-left:15px;">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("amount") . ': ' . $this->tec->formatMoney($payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("change") . ': ' . ($payment->pos_balance > 0 ? $this->tec->formatMoney($payment->pos_balance) : 0) . '</td>';
                    }
                    if (($payment->paid_by == 'CC' || $payment->paid_by == 'ppp' || $payment->paid_by == 'stripe') && $payment->cc_no) {
                        echo '<td style="padding-left:15px;">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("amount") . ': ' . $this->tec->formatMoney($payment->pos_paid) . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("no") . ': ' . 'xxxx xxxx xxxx ' . substr($payment->cc_no, -4) . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("name") . ': ' . $payment->cc_holder . '</td>';
                    }
                    if ($payment->paid_by == 'Cheque' && $payment->cheque_no) {
                        echo '<td style="padding-left:15px;">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("amount") . ': ' . $this->tec->formatMoney($payment->pos_paid) . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("cheque_no") . ': ' . $payment->cheque_no . '</td>';
                    }
                    if ($payment->paid_by == 'gift_card' && $payment->pos_paid) {
                        echo '<td style="padding-left:15px;">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("no") . ': ' . $payment->gc_no . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("amount") . ': ' . $this->tec->formatMoney($payment->pos_paid) . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("balance") . ': ' . ($payment->pos_balance > 0 ? $this->tec->formatMoney($payment->pos_balance) : 0) . '</td>';
                    }
                    if ($payment->paid_by == 'other' && $payment->amount) {
                        echo '<td style="padding-left:15px;">' . lang("paid_by") . ': ' . lang($payment->paid_by) . '</td>';
                        echo '<td style="padding-left:15px;">' . lang("amount") . ': ' . $this->tec->formatMoney($payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . '</td>';
                        echo $payment->note ? '</tr><td colspan="2">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                    }
                    echo '</tr>';
                }
                echo '</tbody></table>';
            }

            ?>

            <?= $inv->note ? '<p style="margin-top:10px; text-align: center;">' . $this->tec->decode_html($inv->note) . '</p>' : ''; ?>
            <div class="well well-sm"  style="margin-top:10px;">
                <div style="text-align: center;"><?= $store->receipt_footer; ?></div>
                <div style="text-align: center;"><?= $Settings->footer; ?></div>
            </div>
        </div>
        <div style="clear:both;"></div>
    </div>
<?php if ($modal) {
    echo '</div></div></div></div>';
} else { ?>
<div id="buttons" style="padding-top:10px; text-transform:uppercase;" class="no-print">
    <hr>
    <?php if ($message) { ?>
    <div class="alert alert-success">
        <button data-dismiss="alert" class="close" type="button">×</button>
        <?= is_array($message) ? print_r($message, true) : $message; ?>
    </div>
<?php } ?>

    <?php if ($Settings->java_applet) { ?>
        <span class="col-xs-12"><a class="btn btn-block btn-primary" onClick="printReceipt()"><?= lang("print"); ?></a></span>
        <span class="col-xs-12"><a class="btn btn-block btn-info" type="button" onClick="openCashDrawer()"><?= lang('open_cash_drawer'); ?></a></span>
        <div style="clear:both;"></div>
    <?php } else { ?>
        <span class="pull-right col-xs-12">
        <a href="javascript:window.print()" id="web_print" class="btn btn-block btn-primary"
           onClick="window.print();return false;"><?= lang("web_print"); ?></a>
    </span>
    <?php } ?>
    <!-- <span class="pull-left col-xs-12"><a class="btn btn-block btn-success" href="#" id="email"><?= lang("email"); ?></a></span> -->

    <!-- <span class="col-xs-12">
        <a class="btn btn-block btn-warning" href="<?= site_url('pos'); ?>"><?= lang("back_to_pos"); ?></a>
    </span> -->
    <?php if (!$Settings->java_applet) { ?>
        <div style="clear:both;"></div>
        <div class="col-xs-12" style="background:#F5F5F5; padding:10px;">
            <p style="font-weight:bold;">Please don't forget to disble the header and footer in browser print settings.</p>

            <p style="text-transform: capitalize;"><strong>FF:</strong> File &gt; Print Setup &gt; Margin &amp; Header/Footer Make all --blank--</p>

            <p style="text-transform: capitalize;"><strong>chrome:</strong> Menu &gt; Print &gt; Disable Header/Footer in Option &amp; Set Margins to None</p></div>
    <?php } ?>
    <div style="clear:both;"></div>

</div>

</div>
<canvas id="hidden_screenshot" style="display:none;">

</canvas>
<div class="canvas_con" style="display:none;"></div>
<script src="<?= $assets ?>plugins/jQuery/jQuery-2.1.4.min.js" type="text/javascript"></script>
<?php if ($Settings->java_applet) {

        function drawLine($Settings)
        {
            $size = $Settings->char_per_line;
            $new = '';
            for ($i = 1; $i < $size; $i++) {
                $new .= '-';
            }
            $new .= ' ';
            return $new;
        }

        function printLine($str, $Settings, $sep = ":", $space = NULL)
        {
            $size = $space ? $space : $Settings->char_per_line;
            $lenght = strlen($str);
            list($first, $second) = explode(":", $str, 2);
            $new = $first . ($sep == ":" ? $sep : '');
            for ($i = 1; $i < ($size - $lenght); $i++) {
                $new .= ' ';
            }
            $new .= ($sep != ":" ? $sep : '') . $second;
            return $new;
        }

        function printText($text, $Settings)
        {
            $size = $Settings->char_per_line;
            $new = wordwrap($text, $size, "\\n");
            return $new;
        }

        function taxLine($name, $code, $qty, $amt, $tax)
        {
            return printLine(printLine(printLine(printLine($name . ':' . $code, '', 18) . ':' . $qty, '', 25) . ':' . $amt, '', 35) . ':' . $tax, ' ');
        }

        ?>
        <script type="text/javascript" src="<?= $assets ?>plugins/qz-tray/dependencies/rsvp-3.1.0.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>plugins/qz-tray/dependencies/sha-256.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>plugins/qz-tray/qz-tray.js"></script>
        <script type="text/javascript">
            qz.websocket.connect();

            function printReceipt() {

                receipt = "";
                receipt += chr(27) + chr(69) + "\r" + chr(27) + "\x61" + "\x31\r";
                receipt += "<?= printText(strip_tags(preg_replace('/\s+/',' ', $Settings->header)), $Settings); ?>" + "\n";
                receipt += "\r\n";
                receipt += "<?= printText($store->name.' ('.$store->code.')', $Settings);?>\r\n";
                receipt += "<?= printText($store->city, $Settings);?>\r\n";
                receipt += "<?= printText($store->phone, $Settings);?>\r\n";
                receipt += "\x1B\x61\x31";
                receipt += "<?=drawLine($Settings);?>\r\n";
                receipt += "\x1B\x61\x30";
                receipt += "<?= lang("sale_no") . ": " . $inv->id; ?>" + "\n";
                receipt += "<?= lang("sales_person") . ": " . $created_by->first_name." ".$created_by->last_name; ?>" + "\n";
                receipt += "<?= lang("customer") . ": " . $inv->customer_name; ?>" + "\n";
                receipt += "<?= lang("date") . ": " . $this->tec->hrld($inv->date); ?>" + "\n\n";
                receipt += "<?php $r = 1;
            foreach ($rows as $row): ?>";
                receipt += "<?= "#" . $r ." "; ?>";
                receipt += "<?= product_name(addslashes($row->product_name)); ?>" + "\n";
                receipt += "<?= printLine("   ".$this->tec->formatNumber($row->quantity)."x".$this->tec->formatMoney($row->net_unit_price+($row->item_tax/$row->quantity)) . ":  ". $this->tec->formatMoney($row->subtotal), $Settings, ' ') . ""; ?>" + "\n";
                receipt += "<?php $r++;
            endforeach; ?>";
                receipt += "\x1B\x61\x31";
                receipt += "<?=drawLine($Settings);?>\r\n";
                receipt += "\x1B\x61\x30";
                receipt += "<?= printLine(lang("total") . ": " . $this->tec->formatMoney($inv->total+$inv->product_tax), $Settings); ?>" + "\n";
                <?php if ($inv->order_tax != 0) { ?>
                receipt += "<?= printLine(lang("tax") . ": " . $this->tec->formatMoney($inv->order_tax), $Settings); ?>" + "\n";
                <?php } ?>
                <?php if ($inv->total_discount != 0) { ?>
                receipt += "<?= printLine(lang("discount") . ": " . $this->tec->formatMoney($inv->total_discount), $Settings); ?>" + "\n";
                <?php } ?>
                <?php if($Settings->rounding) { ?>
                receipt += "<?= printLine(lang("rounding") . ": " . $rounding, $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("grand_total") . ": " . $this->tec->formatMoney($inv->grand_total + $rounding), $Settings); ?>" + "\n";
                <?php } else { ?>
                receipt += "<?= printLine(lang("grand_total") . ": " . $this->tec->formatMoney($inv->grand_total), $Settings); ?>" + "\n";
                <?php } ?>
                <?php if($inv->paid < $inv->grand_total) { ?>
                receipt += "<?= printLine(lang("paid_amount") . ": " . $this->tec->formatMoney($inv->paid), $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("due_amount") . ": " . $this->tec->formatMoney($inv->grand_total-$inv->paid), $Settings); ?>" + "\n\n";
                <?php } ?>
                <?php
                if($payments) {
                    foreach($payments as $payment) {
                        if ($payment->paid_by == 'cash' && $payment->pos_paid) { ?>
                receipt += "<?= printLine(lang("paid_by") . ": " . lang($payment->paid_by), $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("amount") . ": " . $this->tec->formatMoney($payment->pos_paid), $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("change") . ": " . ($payment->pos_balance > 0 ? $this->tec->formatMoney($payment->pos_balance) : 0), $Settings); ?>" + "\n";
                <?php } if (($payment->paid_by == 'CC' || $payment->paid_by == 'ppp' || $payment->paid_by == 'stripe') && $payment->cc_no) { ?>
                receipt += "<?= printLine(lang("paid_by") . ": " . lang($payment->paid_by), $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("amount") . ": " . $this->tec->formatMoney($payment->pos_paid), $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("card_no") . ": xxxx xxxx xxxx " . substr($payment->cc_no, -4), $Settings); ?>" + "\n";
                <?php  } if ($payment->paid_by == 'gift_card') { ?>
                receipt += "<?= printLine(lang("paid_by") . ": " . lang($payment->paid_by), $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("amount") . ": " . $this->tec->formatMoney($payment->pos_paid), $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("card_no") . ": " . $payment->gc_no, $Settings); ?>" + "\n";
                <?php } if ($payment->paid_by == 'Cheque' && $payment->cheque_no) { ?>
                receipt += "<?= printLine(lang("paid_by") . ": " . lang($payment->paid_by), $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("amount") . ": " . $this->tec->formatMoney($payment->pos_paid), $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("cheque_no") . ": " . $payment->cheque_no, $Settings); ?>" + "\n";
                <?php if ($payment->paid_by == 'other' && $payment->amount) { ?>
                receipt += "<?= printLine(lang("paid_by") . ": " . lang($payment->paid_by), $Settings); ?>" + "\n";
                receipt += "<?= printLine(lang("amount") . ": " . $this->tec->formatMoney($payment->amount), $Settings); ?>" + "\n";
                receipt += "<?= printText(lang("payment_note") . ": " . $payment->note, $Settings); ?>" + "\n";
                <?php }
            }

        }
    }

                ?>
                receipt += "\x1B\x61\x31";
                receipt += "\r\n";
                <?php if ($inv->note) { ?>
                receipt += "<?= printText(strip_tags(preg_replace('/\s+/',' ', $this->tec->decode_html($inv->note))), $Settings); ?>" + "\n";
                <?php } ?>
                receipt += "<?= printText(strip_tags(preg_replace('/\s+/',' ', $store->receipt_footer)), $Settings); ?>" + "\n";
                receipt += "<?= printText(strip_tags(preg_replace('/\s+/',' ', $Settings->footer)), $Settings); ?>" + "\n";
                receipt += "\x1B\x61\x30";

                receipt += "\r\n";
                receipt += "\r\n";
                receipt += "\r\n";
                receipt += "\r\n";
                receipt += "\x1B\x40";
                receipt += "\r\n";
                receipt += "\r\n";

                receipt += (chr(27) + chr(105)); 

                <?php if(isset($Settings->cash_drawer_cose)) { ?>
                    receipt += '<?=$Settings->cash_drawer_cose;?>');
                <?php } else { ?>
                    receipt += (chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
                <?php } ?>

                // Tray
                qz.printers.find("<?=$Settings->receipt_printer?>").then(function(printer) {
                    var config = qz.configs.create(printer, {jobName: "Receipt #<?= $inv->id; ?>"});
                    var data = [
                        // { type:'image', data: '<?= base_url('uploads/'.$store->logo);?>' },
                        { type: 'raw', data: receipt },
                    ];
                    return qz.print(config, data);
                }).catch(function(e) { console.error(e); });

               //  var config = qz.configs.create("<?=$Settings->receipt_printer?>", { jobName: "Receipt #<?= $inv->id; ?>" });
               //  var html = $('#receipt-data').html();
               //  var data = [{
               //     type: 'html',
               //     format: 'plain', // or file
               //     data: html
               // }];
               // qz.print(config, data).catch(function(e) { console.error(e); });

            }

            function chr(i) {
                return String.fromCharCode(i);
            }

            function cutPaper() {
                qz.append(chr(27) + chr(105));   
            }
                    
            function openCashDrawer() {
                var receipt = "";
                <?php if(isset($Settings->cash_drawer_cose)) { ?>
                    receipt += '<?=$Settings->cash_drawer_cose;?>');
                <?php } else { ?>
                    receipt += (chr(27) + "\x70" + "\x30" + chr(25) + chr(25) + "\r");
                <?php } ?>
                receipt += "\x1B\x40";

                // Tray
                qz.printers.find("<?=$Settings->receipt_printer?>").then(function(printer) {
                    var config = qz.configs.create(printer);
                    var data = [receipt];
                    return qz.print(config, data);
                }).catch(function(e) { console.error(e); });
            }
        </script>
    <?php } ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('#email').click(function () {
                        var email = prompt("<?= lang("email_address"); ?>", "<?= $customer->email; ?>");
                        if (email != null) {
                            $.ajax({
                                type: "post",
                                url: "<?= site_url('pos/email_receipt') ?>",
                                data: {<?= $this->security->get_csrf_token_name(); ?>: "<?= $this->security->get_csrf_hash(); ?>", email: email, id: <?= $inv->id; ?>},
                                dataType: "json",
                                success: function (data) {
                                    alert(data.msg);
                                },
                                error: function () {
                                    alert('<?= lang('ajax_request_failed'); ?>');
                                    return false;
                                }
                            });
                        }
                        return false;
                    });
                });
        <?php if (!$Settings->java_applet && !$noprint) { ?>
        $(window).load(function () {
            window.print();
        });
    <?php } ?>
            </script>
</body>
</html>
<?php } ?>
